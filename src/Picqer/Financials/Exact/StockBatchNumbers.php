<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 19/05/17
 * Time: 16:51
 */

namespace Picqer\Financials\Exact;


class StockBatchNumbers extends Model
{
    protected $primaryKey = 'ID';

    use Query\Findable;
    use Persistance\Storable;

    protected $fillable = [
        'ID',
        'BatchNumber',
        'Item',
        'ItemCode',
        'ItemDescription',
        'BatchNumberID',
        'StockTransactionID',
        'StockTransactionType',

    ];

    protected $url = 'inventory/StockBatchNumbers?&$select=BatchNumber,ID,Item,ItemCode,ItemDescription,BatchNumberID,StockTransactionID,StockTransactionType';

}