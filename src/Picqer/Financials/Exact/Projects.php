<?php namespace Picqer\Financials\Exact;

class Projects extends Model
{

    use Query\Findable;
    use Persistance\Storable;

    protected $fillable = [
        'ID',
        'Account',
        'AccountCode',
        'AccountContact',
        'AccountName',
        'BudgetedCosts',
        'BudgetedAmount',
        'BudgetedCosts',
        'Code',
        'CostsAmountFC',
        'CustomerPOnumber',
        'Description',
        'FixedPriceItem',
        'Type',
        'TypeDescription',
        'EndDate',
        'InvoiceTerms',
        'ManagerFullname',
        'Notes',
        'PrepaidItem',
        'SalesTimeQuantity',
        'StartDate',
        'UseBillingMilestones',
        'ClassificationDescription',
    ];

    protected $url = 'project/Projects';

}
