<?php
namespace Picqer\Financials\Exact;

class GoodsDeliveryLines extends Model
{
    use Query\Findable;
    use Persistance\Storable;

    protected $primaryKey = 'ID';

    protected $fillable = [
        'ID',
        'BatchNumbers',
        'Created',
        'Creator',
        'CreatorFullName',
        'DeliveryDate',
        'Description',
        'Division',
        'EntryID',
        'Item',
        'ItemCode',
        'ItemDescription',
        'LineNumber',
        'Modified',
        'Modifier',
        'ModifierFullName',
        'Notes',
        'QuantityDelivered',
        'QuantityOrdered',
        'SalesOrderLineID',
        'SalesOrderLineNumber',
        'SalesOrderNumber',
        'SerialNumbers',
        'StorageLocation',
        'StorageLocationCode',
        'StorageLocationDescription',
        'TrackingNumber',
        'Unitcode',
    ];

    protected $url = 'salesorder/GoodsDeliveryLines?&$select= ID,BatchNumbers,Created,Creator,CreatorFullName,DeliveryDate,Description,Division,EntryID,Item,ItemCode,ItemDescription,LineNumber,Modified,Modifier,ModifierFullName,Notes,QuantityDelivered,QuantityOrdered,SalesOrderLineID,SalesOrderLineNumber,SalesOrderNumber,SerialNumbers,StorageLocation,StorageLocationCode,StorageLocationDescription,TrackingNumber,Unitcode,';

}